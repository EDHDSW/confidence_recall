#  Recall weights and Confidence in Neural Networks

We  explore two concepts:

**Recall weights**<br>
The Neural network divides the inputs according the difference between the input vector and a representation vector , each of this divisions have a different first layer set of weights that helps the neural network to converge better to the result<br>
**Confidence** <br>
The Neural network computes a confidence value as the same time that process the inputs it represents the similarity of previously seen inputs againts the evaluation input<br>


## Model

We will use the regression model from the Baseline.ipynb nootebok because it was the case with the poorest performance because the data doesnt cover all the search space  (only 1000 samples) <br>
The function to regress is **(x1 - x2) + (x3 * x3)** 
All the inputs and outputs are normalized between **(-1,1 )** <br>

#### Model Pipeline
**Training** <br>
Inputs --> normalize&Cliping --> compute Representation_Vector --> Recall Weights --> model --Output--> loss --> update weights -->update recall weights

**Evaluation** <br>
Inputs --> normalize&Cliping --> compute Representation_Vector --> Recall Weights --> model --Output--> ask for supervision if required--> update recall weights--> denormalize output

## Representation vector 

First step is to compute a vector that represent the training set  in the case of this model it will be the mean of each input of the training set so: <br>
**Vr = [X0mean, X1 mean ,X3mean]**

The representation vector can be any mathematical model that characterize well the data (weighted mean, trigonometric expresion...)

## Recall weights

The recall weights are the first layer of a neural network model.
The difference between  the representation vector(**Vr**) and the input vector (**Vx**) will create divisions , each of this divisions will have a recall weight associated. For this baseline we will use [cosine similarity](https://pytorch.org/docs/stable/nn.html?highlight=cosine#torch.nn.CosineSimilarity) as the mesasurement of the diference between two vectors:

**Vdiff = cosine_similarity (Vr,Vx)**

Vdiff can be computed with another distance representation as angular distance, etc

## Divisions

If the difference between the vectors is greather than the  hyperparameter  called **eps**  ( in the notebook eps is 0.1) <br>
A new division and a new set of wights will be created , each division will keep track of the prediction (Y) associated to that division<br> 
Each Vector difference (Vdiff) and prediction(Y) of the input will be meaned with the Vdiff and Y that represent the division so each division will have a Vdiffm and Ymean :

**Divisions  = (Vdiff-n * ESP, Vdiff + n * ESP) , n = 0,1,2,3... <br>
Vdiffm = (Vdiffm + Vdiff) /2  <br>
Ymean = ( Ymean + Y )/2**


## Confidence

The confidence meter doesn't represent the correctness of the prediction. The confidence meter tries to emulate the confidence express by a person during a decision , the most the current situation  looks like a previous situation the most confidence is expresed by the person.<br>
The confidence meter could be use to ask for supervision to a user or to another algorithm  if the confidence falls below a threshold 

Confidence is expressed by the difference  of the difference vector(Vdiff) and the difference vector mean of each division(Vdiffm) plus the difference of the mean output associated to that division  (Ymean) and the prediction (Y)

**C = 1 -( abs(Vdiffm -Vdiff) + abs(Ymean - Y) )**

## Training 

Train the model is performed in two steps :
1. Train the model without generating the recall weights this will give us a baseline for the recall weights(first layers of the model)
2. Train the model only updating the recall weights wiht a lower learning rate, this will update the weights according their vector difference

## Evaluation 

Evaluation can be performed with or without supervision , when supervision enable  if the confidence of the prediction falls from a deifned threshold the model will ask for supervsion and it will update the recall weights only accordingly the feedback from the supervision routine output 

## Prerequisites
- Python 3 
- Pytorch
- Jupyter noteboks enviroment




## Running
Donwload and execute the notebooks the cvs data is included 
